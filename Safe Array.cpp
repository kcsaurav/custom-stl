#include <iostream>
#include <algorithm> // possibly not required

using namespace std;

template< class T >
class SA {
 private:
	int low, high;
	T* p;
 public:
	SA(){ // default constructor
		low = 0;
		high = -1;
		p = NULL;
	}

	SA(int i) { // 1 parameter constructor
		low = 0;
		high = i - 1;
		p = new T[i];
	}
	
	SA(int l, int h){ // 2 parameter constructor
		if ((h - l + 1) <= 0){
			cout << "constructor error in bounds definition" << endl;
			exit(1);
		}
		low = l;
		high = h;
		p = new T[h - l + 1];
	}

	SA(const SA<T> & s) { // copy constructor
		int size = s.high - s.low + 1;
		p = new T[size];
		for (int i = 0; i<size; i++)
			p[i] = s.p[i];
		low = s.low;
		high = s.high;
	}

	~SA() {	delete[] p; } // destructor
	
	T& operator[](int i) { // overloaded [] operator
		if (i<low || i>high){
			cout << "index " << i << " out of range" << endl;
			exit(1);
		}
		return p[i - low];
	}

	SA & operator=(const SA<T> & s) { // overloaded assignment
		if (this == &s)return *this;
		delete[] p;
		int size = s.high - s.low + 1;
		p = new T[size];
		for (int i = 0; i<size; i++)
			p[i] = s.p[i];
		low = s.low;
		high = s.high;
		return *this;
	}

	friend ostream& operator<<(ostream& os, SA<T> s) { // overloaded << to print SAs
		int size = s.high - s.low + 1;
		for (int i = 0; i < size; i++)
			cout << s.p[i] << " ";
		return os;
	}

	T* begin(){	return &p[0]; }

	T* end(){
		int size = this->high - this->low + 1;
		return &p[size];
	}
};
  // Sort SA in ascending order
	bool sortinAscend(const int a, const int b) { return a <= b; }

	template<typename T>
	void sort(SA<T>& arr){
		sort(arr.begin(),arr.end(),sortinAscend);
	}

	template<typename T>
	void find(SA<T>& arr, int num){
		T* it = find(arr.begin(), arr.end(), num);
		if( it != arr.end() ){ // index start at 0 but counting from 1
			cout << num <<" found at position : " << it - arr.begin() + 1  << endl; 
		}
    else cout << num << " is not found.\n";
	}

	int main(){
		SA<int> a(10), b(3,5);
		b[3] = 3;
		b[4] = 4;
		b[5] = 5;
		int i;
		for( i = 0; i < 10; i++) a[i]=10-i;
		cout<<"printing A the first time" << endl;
		cout<< a << endl;
		cout<<"printing using []"<< endl;
		for( i = 0; i < 10; i++) cout<< a[i] << " ";
		cout<< endl;

		find(a,7);
		sort(a); // write your own sort
		cout<<"printing A using sort" << endl;
		cout<< a << endl;
		b[4] = 12;
		cout<< "printing B " << endl;
		cout<< b << endl;
		find(b,10);
		find(b,12);
		a[10] = 12; // should print an error message and exit
		return 0; 
	}
