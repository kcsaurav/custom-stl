// safe_matrix.cpp
// Templatized 2d safe array (Matrix) class with tests for matrix add/multiplication
// Created by SAURAV KC
#include <iostream> 
#include <cstdlib>
using namespace std;

template <typename T>
class SA{ // SA class ~ overloaded << op.
 private:
  int low, high;
  T* p; // points to SA data type
  
 public:
    SA(){ // default constructor allows for writing things like SA a;
      low = 0;
      high = -1;
      p = NULL;
    }
    
    // 2 parameter constructor lets us write SA x(10,20);
    SA(int l, int h){
      if( ( h - l + 1 ) <= 0 ){
        cout << "constructor error in bounds definition" << endl;
        exit(1);
      }
      low = l;
      high = h;
      p = new T[ h - l + 1 ];
    }
    
    // single parameter constructor lets us create a SA almost like a "standard" one
    // by writing SA x(10); and getting an array x indexed from 0 to 9
    SA(int i){
      low = 0;
      high = i - 1;
      p = new T[i];
    }
    
    // copy constructor for pass by value and initialization
    SA(const SA& s){
      int size = s.high - s.low + 1;
      p = new T[size];
      for(int i = 0; i < size; i++)
        p[i] = s.p[i];
      low = s.low;
      high = s.high;
    }
    
    // destructor
    ~SA(){ delete [] p; }
    
    // overloaded [] lets us write: SA x(10,20); x[15] = 100;
    T& operator[](int i){
      if( i < low || i > high){
        cout << "index " << i << " out of range" << endl;
        exit(1);
      }
      return p[i-low];
    }
    
    SA& operator=(const SA& s){ // overloaded assignment op allows assign to a SA
      if ( this == &s )  return *this;
      
      delete [] p;
      int size = s.high - s.low + 1;
      p = new T[size];
      for ( int i = 0; i < size; i++)
        p[i] = s.p[i];
      low = s.low;
      high = s.high;
      return *this;
    }         
    
    // overloaded << so we can directly print SAs elements
    friend ostream& operator<<(ostream& os, SA<T>& s){ 
      int size = s.high-s.low+1; 
      for(int i = 0; i < size; i++) 
      os<< s.p[i] << " "; // separated by a space
      return os; 
    }
};

template<typename T>
class Matrix{
 private:
  int xhigh, xlow, yhigh, ylow; // upper lower bounds on each dimensions X/Y
  SA< SA<T> >* mat; // 2d safe array matrix

 public:
  Matrix(){
    xlow = xhigh = ylow = yhigh  = 0;
    mat = NULL;
  }

  Matrix(int xl, int xh, int yl, int yh){
    xlow = xl;
    xhigh = xh;
    ylow = xl;
    yhigh = yh;
    mat = new SA< SA<T> >(xl,xh);
    for(int i = xl; i <= xh; i++){
      SA<T> yVal(yl,yh);
      (*mat)[i] = yVal; // holds array elements
    }
  }
  
  Matrix(Matrix<T>& m ){
    xhigh = m.xhigh;
    xlow = m.xlow;
    yhigh = m.yhigh;
    ylow = m.ylow;
    mat = new SA< SA<T> > (xlow, xhigh);
    for(int i = xlow; i<= xhigh ; i++){
      (*mat)[i] = m[i]; // copies every passed underlying SA
    }
  }

  Matrix<T>& operator+(Matrix<T>& x ){
    if(xhigh - xlow + 1 != x.xhigh - x.xlow + 1 && yhigh - ylow + 1 != x.yhigh - x.ylow + 1){
      cout << "Cannot add different sized matrices!\n";
      exit(1);
    }
    Matrix<T> *ans = new Matrix<T> (x);
    int row = xhigh - xlow + 1;
    int col = yhigh - ylow + 1;

    for(int i = 0; i< row; i++){
      SA<T> temp = (*mat)[xlow + i];
      for(int j = 0; j< col; j++){
          (*ans)[ans->xlow+i][ans->ylow+j] += temp[ylow+j];
      }
    }
    return *ans;
  }

  Matrix<T>& operator*(Matrix<T>& x ){
    if(yhigh - ylow+1 != x.xhigh-x.xlow+1){ // Check if 1st matrix col = 2nd's row
      cout << "Cannot multiply these 2 matrices!\n";
      exit(1);
    }
    int row = xhigh - xlow;
    int col = x.yhigh - x.ylow;
    Matrix<T> *prod = new Matrix<T> (0, row, 0 ,col);

    int size = yhigh - ylow +1;
    for(int i = 0; i<= row; i++){
      for(int j = 0; j<= col; j++){
        (*prod)[i][j] = 0;
        for(int k = 0; k<size; k++){
          (*prod)[i][j] += (*mat)[xlow+i][ylow+k] * (*x.mat)[x.xlow+k][x.ylow+j];
        }
      }
    }
    return *prod;
  }

  SA<T>& operator[](int i){ return (*mat)[i]; }
  SA<T>& getSA(int i){ return (*mat)[i]; } // get array elements for printing

  ~Matrix(){ delete mat; }

  template<class U>friend ostream& operator<<(ostream& os, Matrix<U>& m);
  // allows stream access to pvt members of matrix
};

// Overloaded << operator to print a SA matrix
template<typename T>
ostream& operator<<(ostream& os, Matrix<T>& m){
  for(int i = m.xlow; i <= m.xhigh; i++) 
    os<< m.getSA(i) << endl; // The safe array
  return os; 
}

int main(){
  Matrix<int> x (0,1,0,1);
  x[0][0] = 1; x[0][1] = 2; 
  x[1][0] = 3; x[1][1] = 4;
  
  cout << "Matrix X:\n";
  cout<< x << endl;

  Matrix<int> y (2,3,2,3);
  y[2][2] = 5; y[2][3] = 6;
  y[3][2] = 7; y[3][3] = 8;

  cout << "Matrix Y:\n";
  cout<< y << endl;

  Matrix<int> z = x + y; // Addition test
  cout << "(X + Y):\n";
  cout<< z << endl;

  z = x * y;
  cout << "(X * Y):\n";
  cout<< z << endl;

  Matrix<int> a (0,1,0,2);
  a[0][0] = 1; a[0][1] = 2; a[0][2] = 3;
  a[1][0] = 4; a[1][1] = 5; a[1][2] = 6;
  
  cout << "Matrix A:\n";
  cout<< a << endl;

  Matrix<int> b (0,2,0,1);
  b[0][0] = 7; b[0][1] = 8;
  b[1][0] = 9; b[1][1] = 10;
  b[2][0] = 11; b[2][1] = 12;

  cout << "Matrix B:\n";
  cout<< b << endl;

  z = a * b; // a+b;
  cout << "(A * B):\n";
  cout<< z << endl;
  
  return 0; 
}